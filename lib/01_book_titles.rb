class Book
  attr_accessor :title
  def title
    title = cap_everyone(@title)
    title = fix_conjunctions(title)
    title = fix_prepostions(title)
    title = fix_articles(title)
    cap_the_first_guy!(title)
    title.join(" ")
  end

  def cap_everyone(str)
    str.split.map(&:capitalize)
  end

  def fix_conjunctions(str_arr)
    conjunctions = ["for", "and", "nor", "but", "or", "so", "yet"]
    str_arr.map do |word|
      if conjunctions.include? word.downcase
        word.downcase
      else word
      end
    end

  end

  def fix_prepostions(str_arr)
    prepositions = ["in", "of"]
    str_arr.map do |word|
      if prepositions.include? word.downcase
        word.downcase
      else word
      end
    end
  end

  def fix_articles(str_arr)
    articles = ["a", "an", "the"]
    str_arr.map do |word|
      if articles.include? word.downcase
        word.downcase
      else word
      end
    end
  end

  def cap_the_first_guy!(str_arr)
    str_arr[0] = str_arr[0].capitalize
  end


end
