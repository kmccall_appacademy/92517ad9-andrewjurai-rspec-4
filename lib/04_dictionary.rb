class Dictionary

  def initialize
    @entries = Hash.new
  end

  def add(entry)
    keyword = get_keyword_and_definition_from(entry)[0]
    definition = get_keyword_and_definition_from(entry)[-1]
    @entries[keyword] = definition
  end

  def include?(keyword)
    keywords.include?(keyword)
  end

  def find(keyword)
    return @entries if entries.empty?
    similar_keys = keywords.select { |word| word if word.include?(keyword) }
    possible_entires = Array.new
    similar_keys.each { |key| possible_entires << [key, @entries[key]] }
    possible_entires.to_h
  end

  def entries
    @entries
  end

  def keywords
    @entries.keys.sort
  end

  def get_keyword_and_definition_from(entry)
    if entry.class == Hash
      entry.to_a.flatten
    else [entry, nil]
    end
  end

  def printable
    entries_nested = @entries.to_a
    print_arr = Array.new
    entries_nested.each { |nest| print_arr << format_entry(nest) }
    print_arr.sort.join("\n")
  end

  def format_entry(nest)
    %Q{[#{nest[0]}] "#{nest[1]}"}
  end

end
