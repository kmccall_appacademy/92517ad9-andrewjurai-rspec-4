class Temperature

  def initialize(arg)
    if self.class == Temperature
      @temperature = arg.values[0]
      @units = arg.keys[0]
    elsif self.class == Celsius
      @temperature = arg
      @units = :c
    elsif self.class == Fahrenheit
      @temperature = arg
      @units = :f
    end
  end

  def in_fahrenheit
    return @temperature if @units == :f
    @temperature * (9.0 / 5.0) + 32
  end

  def in_celsius
    return @temperature if @units == :c
    (@temperature - 32.0) * (5.0 / 9.0)
  end

  def self.from_celsius(temperature)
    Temperature.new(:c => temperature)
  end

  def self.from_fahrenheit(temperature)
    Temperature.new(:f => temperature)
  end

end


class Celsius < Temperature
end

class Fahrenheit < Temperature
end
