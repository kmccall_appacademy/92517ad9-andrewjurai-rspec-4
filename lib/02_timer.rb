class Timer

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    formated_time.join(":")
  end

  def formated_time
    [[hrs], [mins], [secs]].map do |value|
      value.unshift(0) if value[0] < 9
      value.join
    end
  end

  def hrs
    @seconds / 3600
  end

  def mins
    (@seconds - hrs * 3600) / 60
  end

  def secs
    @seconds - hrs * 3600 - mins * 60
  end

end
